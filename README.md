## Docker image dedicated to OpenShift to be used in GitLab CI pipelines.

This image contains:
- The OpenShift client (oc) version 3.6.1
- ocgoss
- oc-create-or-update-app
- oc-batch
- oc-wait
- gitlab-start-manual-job
- gitlab-trigger-pipeline
- modify-template-for-branch

### Testing the Docker image using ocgoss

Docker image tests on OpenShift are done using `ocgoss`.  
`ocgoss` is a tooling around [goss](https://goss.rocks) that will start the docker image inside OpenShift and copy the goss binary and the test file inside.  
`ocgoss` acts like [`dgoss`](https://github.com/aelsabbahy/goss/tree/master/extras/dgoss) for docker.

You can use it like this in your `.gitlab-ci.yml`:

```
image: registry-innersource.soprasteria.com/software-automation-architecture/ci/oc-ci

test:
  stage: test
  script:
  - ocgoss <image name>
```

You can use a `goss_wait.yaml` file in order to tell `dgoss` to wait until the application in the container is really started.  
For example, for a web application:
```
http:
  http://localhost:8080/:
    status: 200
```
It will wait until the following url answers 200.

Add all the tests in the `goss.yaml` file. You can fill it with all the [commands allowed by `goss`](https://github.com/aelsabbahy/goss/blob/master/docs/manual.md).  
Here is a test example:
```
port:
  tcp:4447:
    listening: true

http:
  http://localhost:8080/:
    status: 200
    body:
    - Welcome to JBoss EAP 6

file:
  /home/jboss/log/server.log:
    exists: true
    filetype: file
    contains:
    - started in

command:
  java -version:
    exit-status: 0
    stderr:
    - java version "1.8
```

### Additional options

If you provide a `goss_wait.yaml` file, by default, the test will wait 1 minute before starting. If it's too short, you can provide two parameters:
- `RETRY_TIMEOUT`: Retry on failure so long as elapsed + sleep time is less than this (default: 240s)
- `SLEEP_BETWEEN_RETRIES`: Time to sleep between retries (default: 2s)

Here is an example to wait 4 minutes before the test starts (It waits only if you provide a `goss_wait.yaml` file):
```
  script:
  - RETRY_TIMEOUT=240 ocgoss <image name>
```

### Create or update OpenShift application

The `oc-create-or-update-app` command creates or updates an OpenShift application based on a Template.  
For each object of the template the command check if it's necessary to create or update it.  
You need to be in the right OpenShift project before starting the command.  

The command has the following arguments:  

- `file` (mandatory): an Openshift template with all objects to create or update
- `app` (mandatory): the name of the application
- `param` (optional): parameters to override inside the given template

An example:

```sh
$ oc project sa2
$ oc-create-or-update-app --app ${APP} --file documentation-template.yml
```

#### Known issues:

- You need to manually delete objects whose type is no longer present in the deployment  
  For example if there was a ServiceAccount in the deployment and now there is none, the remaining ServiceAccount will not be deleted

- In the deployment file, arrays of objects must remain in the same order otherwise when the deployed configuration is merged to the new configuration the bad object will be overwritten.

### Execute Batch Process

The `oc-batch` command allows to easily execute a batch process on Openshift. A batch process is an operation that will interact with the deployed application for a delimited amount of time, in other words it should not always be running. Some examples: end of day processing, import of data, cleaning operation... 

The command executes a template file where a pod object should be defined (this pod will be responsible to run the batch process), then it waits for the pod to finish (see section [below](#wait-for-a-pod-to-finish)).

The command has the following arguments:

* `file` (mandatory): an Openshift template with a pod object corresponding to the batch to execute
* `batch` (mandatory): the name of the pod batch
* `app` (mandatory): the name of the application
* `param` (optional): parameters to override inside the given template

An example:
```sh
$ oc project sa2
$ oc-batch --app test --file batch-job.yml --param BATCH=eod_batch --param ARG=end_of_day.sh --batch=eod_batch
```
batch-job.yml
```yaml
kind: Template
apiVersion: v1
metadata:
  annotations:
    description: The batch job template
  name: "batch"
objects:
- apiVersion: v1
  kind: Pod
  metadata:
    name: ${BATCH}
  spec:
    containers:
    - name: ${BATCH}
      image: "docker-registry.default.svc:5000/sa2-frmorel/batch"         
      imagePullPolicy: "Always"
      command: ["sh", "${ARG}"]
    restartPolicy: Never
parameters:
- name: BATCH
  displayName: application name
  description: The name of the application
  value: batch
- name: ARG
  displayName: argument
  description: argument to pass to the executable
  value: batch.sh
```

### Wait for a Pod to finish

The `oc-wait`command waits until the pod has finished by following the logs then checks the status of the pod and exit 0 if everything went well or 1 in case of error.

The command has the following argument:

* `pod` (mandatory): the pod id

an example:
```sh
$ oc project sa2
$ oc-wait --pod myPod-r7381
```