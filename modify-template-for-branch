#!/usr/bin/env node
//-----------------------------------------------------------------------------
// Modify the template to use images built for the specified branch slug
//-----------------------------------------------------------------------------

const yaml = require('js-yaml')
const { basename } = require('path')
const fs = require('fs')
const { execSync } = require('child_process')
const _ = require('lodash')
const yargs = require('yargs')
const { version } = require('./package')

const options = {
    'template': {
        alias: 't',
        describe: 'Template filename.'
    },
    'output': {
        alias: 'o',
        describe: 'Output filename.'
    },
    'slug': {
        alias: 's',
        describe: 'Branch slug (Mainly the content of $CI_COMMIT_REF_SLUG).'
    }
}

let demandOption = ['template', 'output', 'slug']
const description = 'Modify the template to use images built for the specified branch slug'

const arg = yargs
    .locale('en')
    .wrap(100)
    .usage(`\nUsage: $0 [Options] 
        ${description}`)
    .demandOption(demandOption)
    .options(options)
    .help()
    .argv

const commandName = basename(arg.$0)
console.info(`${commandName} ${version}`)
console.info('')

const template = yaml.safeLoad(fs.readFileSync(arg.template, 'utf8'));

if (arg.slug === 'master') {
    console.info(`No need to modify images in template '${arg.template}' for the 'master' branch. Template dumped unmodified into '${arg.output}'`)
}
else {
    const currentProject = exec('oc project -q')

    const result = JSON.parse(exec(`oc get ImageStreams -o json`))
    const imageStreamNames = _.map(result.items, item => item.metadata.name).sort()
    console.info('Existing Image Streams:', imageStreamNames.join(', '))

    console.info(`Modifying the '${arg.template}' template to use the image(s) built for branch slug '${arg.slug}'`)
    console.info('')

    let modified = false
    traverse(template, (key, value) => {
        if (key === 'from' && value.hasOwnProperty('kind') && value.kind === 'ImageStreamTag' && value.namespace === currentProject) {
            const parts = value.name.split(':')
            let imageName = parts[0]
            let tagName = 'latest'
            if (parts.length > 1) {
                tagName = parts[1]
            }
            const imageNameWithBranch = `${imageName}-${arg.slug}`
            if (imageStreamNames.includes(imageNameWithBranch)) {
                const newName = `${imageNameWithBranch}:${tagName}`
                console.info(`Using image '${newName}' instead of '${value.name}'`)
                value.name = newName
                modified = true
            }
        }
    })
    if (modified) {
        console.info('')
        console.info(`Template '${arg.template}' modified for branch slug '${arg.slug}' and dumped into '${arg.output}'`)
    } else {
        console.info(`Nothing modified in template '${arg.template}'. Template dumped unmodified into '${arg.output}'`)
    }
}

const templateString = yaml.safeDump(template)
writeFile(arg.output, templateString)

//-----------------------------------------------------------------------------

function traverse(o, func) {
    for (const i in o) {
        func.apply(this, [i, o[i]])
        if (o[i] !== null && typeof (o[i]) === "object") {
            traverse(o[i], func)
        }
    }
}

function exec(command) {
    const result = execSync(command)
    return result && result.toString().trim()
}

function writeFile(file, data) {
    return fs.writeFileSync(file, data, { flag: 'w' })
}
