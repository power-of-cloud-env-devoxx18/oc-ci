FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y wget curl bash nodejs python zip && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    apt-get update && \
    apt-get install -y yarn && \
    # Install oc client
    cd /tmp && \
    wget https://github.com/openshift/origin/releases/download/v3.6.1/openshift-origin-client-tools-v3.6.1-008f2d5-linux-64bit.tar.gz -O oc.tgz && \
    tar --overwrite --strip 1 -C /usr/local/bin -zxvf oc.tgz && \
    rm -f oc.tgz

COPY . /usr/local/bin

RUN chmod a+rx /usr/local/bin/*
