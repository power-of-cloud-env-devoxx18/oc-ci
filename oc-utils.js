const yargs = require('yargs')
const { execSync } = require('child_process')
const wait = require('wait-for-stuff')

function argv(description, options, demandOption) {
    const fullOptions = Object.assign({
        'app': {
            alias: 'a',
            describe: 'Name of the application.'
        },
        'file': {
            alias: 'f',
            describe: 'Path of the of the template application file.'
        },
        'param': {
            alias: 'p',
            describe: 'key-value pair (e.g., -p FOO=BAR) to set/override a parameter value in the template.'
        }
    }, options)

    let fullDemandOption = ['app', 'file']
    if (demandOption)
        fullDemandOption = fullDemandOption.concat(demandOption)

    const argv= yargs
        .locale('en')
        .wrap(100)
        .usage(`\nUsage: $0 -a <application name> -f <template file> [-p <key>=<value>]
        ` + description)
        .demandOption(fullDemandOption)
        .options(fullOptions)
        .help()
        .argv

    const final_argv = argv
    let parameterOptions = ''
    if (argv.param) {
        const paramArray = Array.isArray(argv.param) ? argv.param : [argv.param]
        parameterOptions = paramArray.map(param => `-p "${param}"`).join(' ')
    }      
    final_argv.param = parameterOptions
    
    return final_argv
}

function exec(command) {
    const result = execSync(command)
    return result && result.toString().trim()
}

function waitPodCompletion(podId) {
    wait.for.predicate(() => isPodStarted(podId))
    console.info("logs:\n" + exec(`oc logs -f pods/${podId}`))
    return podStatus(podId)
}

function isPodStarted(podId){
    const status = podStatus(podId)
    return status != "ContainerCreating" && status != "Pending"
}

function podStatus(podId){
    const out = exec(`oc get pods/${podId}`)
    const startIndex = out.indexOf("STATUS")
    const results = out.split("\n")[1]
    const status = results.substring(startIndex, results.indexOf(" ", startIndex))
    return status
}

module.exports = {
    argv,
    exec,
    waitPodCompletion
}