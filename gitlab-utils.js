#!/usr/bin/env node

const yargs = require('yargs')
const _ = require('lodash')
const restClient = require('sync-rest-client')
const { execSync } = require('child_process')

let gitlabUrl = undefined

function argv(description, options, demandOption) {
    const fullOptions = Object.assign({
        'url': {
            alias: 'u',
            describe: 'Gitlab Url.',
            default: 'https://gitlab.com'
        },
        'token': {
            alias: 't',
            describe: 'Gitlab API token.'
        },
        'group': {
            alias: 'g',
            describe: 'Project group.'
        },
        'project': {
            alias: 'p',
            describe: 'Project name.'
        },
        'branch': {
            alias: 'b',
            describe: 'Branch name.'
        }
    }, options)

    let fullDemandOption = ['token', 'group', 'project', 'branch']
    if (demandOption)
        fullDemandOption = fullDemandOption.concat(demandOption)

    const arg = yargs
        .locale('en')
        .wrap(100)
        .usage(`\nUsage: $0 [Options] 
        ${description}`)
        .demandOption(fullDemandOption)
        .options(fullOptions)
        .help()
        .argv

    gitlabUrl = arg.url
    restClient.addGlobalHeader('PRIVATE-TOKEN', arg.token)

    return arg
}

function getProjectId(group, projectName) {
    // https://docs.gitlab.com/ee/api/projects.html#list-all-projects
    const projects = gitlabGet('/projects?owned=true')
    var projectId = _.result(_.find(projects, function (project) {
        return project.namespace.kind === 'group' && project.namespace.name === group && project.name === projectName;
    }), 'id');
    return projectId
}

function branchExist(projectId, branchName) {
    try {
        // https://docs.gitlab.com/ee/api/branches.html#get-single-repository-branch
        gitlabGet(`/projects/${projectId}/repository/branches/${branchName}`)
    } catch (error) {
        return false
    }
    return true
}

function createBranch(projectId, branchName) {
    // https://docs.gitlab.com/ee/api/branches.html#create-repository-branch
    return gitlabPost(`/projects/${projectId}/repository/branches?branch=${branchName}&ref=master`)
}

function createPipeline(projectId, branchName) {
    // https://docs.gitlab.com/ee/api/pipelines.html#create-a-new-pipeline
    return gitlabPost(`/projects/${projectId}/pipeline?ref=${branchName}`)
}

function triggerPipeline(projectId, branchName) {
    // https://docs.gitlab.com/ee/ci/triggers/README.html#when-used-with-multi-project-pipelinesspan-classbadge-trigger-premiumspan
    const command = `curl -s --request POST --form "token=${process.env.CI_JOB_TOKEN}" --form ref=${branchName} ${gitlabUrl}/api/v4/projects/${projectId}/trigger/pipeline`
    const result = JSON.parse(exec(command))
    if (_.isObject(result) && result.message) {
        throw new Error(result.message)
    }
    return result
}

function getBranchPipelines(projectId, branchName) {
    // https://docs.gitlab.com/ee/api/pipelines.html#list-project-pipelines
    return gitlabGet(`/projects/${projectId}/pipelines?ref=${branchName}`)
}

function getManualJobs(projectId, pipelineId) {
    // https://docs.gitlab.com/ee/api/jobs.html#list-pipeline-jobs
    return gitlabGet(`/projects/${projectId}/pipelines/${pipelineId}/jobs?scope[]=manual`)
}

function startJob(projectId, jobId) {
    // https://docs.gitlab.com/ee/api/jobs.html#play-a-job
    return gitlabPost(`/projects/${projectId}/jobs/${jobId}/play`)
}

module.exports = {
    argv,
    getProjectId,
    branchExist,
    createBranch,
    createPipeline,
    triggerPipeline,
    getBranchPipelines,
    getManualJobs,
    startJob
}

// Internals ------------------------------------------------------------------

function gitlabGet(resource) {
    var response = restClient.get(`${gitlabUrl}/api/v4${resource}`)
    manageError(response)
    return response.body
}

function gitlabPost(resource, options) {
    var response = restClient.post(`${gitlabUrl}/api/v4${resource}`, options)
    manageError(response)
    return response.body
}

function manageError(response) {
    if (response.statusCode != 200 && _.isObject(response.body) && response.body.message) {
        throw new Error(response.body.message)
    }
}

function exec(command) {
    const result = execSync(command)
    return result && result.toString().trim()
}
